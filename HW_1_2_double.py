#Double random number matrix
import numpy as np
import datetime
A=np.random.rand(100,150)
B=np.random.rand(150,100)

begin = datetime.datetime.now()

colA=(len(A[0]))#Columns of matrix A
rowA=(len(A))#Rows of matrix A
colB=(len(B[0]))#Columns of matrix B
rowB=(len(B))#Rows of matrix B
colC=colB#Columns of matrix C
rowC=rowA#Rows of matrix C
#rowA=colB=rowC
C = [[0] * colC for i in range(rowC)] #initialize C
#print(A)
#print(B)
#Multiplication
for i in range(rowC):
	for j in range(colC):
		#serach every element 
		for k in range(colC):
			C[i][j]+=A[i][k]*B[k][j]
print(C)

end = datetime.datetime.now()
k = end - begin
print(k)
