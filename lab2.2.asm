	.data 0x10010000
var1: 	.word 0x0083 	# var1 is a word (32 bit) with the ..
			# initial value 0x55
var2: 	.word 0x0104
var3:	.word 0x0111
var4:	.word 0x0119
first:	.byte'm'
last:	.byte'c'

	.text
	.globl main

main:	addu $s0, $ra, $0	# save $31 in $16
	li $t0, 8
	move $t1, $t0
	la $t2, var1	# get the address of var1
	la $t5, var4	# get the address of var4
	lw $t4, var1	# save var4 in register $t4
	lw $t3, var4	# save var1 in register $t3
	sw $t3, 0($t2)	# store var4 into var1 address
	sw $t4, 0($t5)	# store var1 into var4 address
	la $t2, var2	# get the address of var2
	la $t5, var3	# get the address of var3
	lw $t4, var2	# save var2 in register $t4
	lw $t3, var3	# save var3 in register $t3
	sw $t3, 0($t2)	# store var3 into var2 address
	sw $t4, 0($t5)	# store var2 into var3 address

# restore now the return address in $ra and return from main
	addu $ra, $0, $s0 	# return address back in $31
	jr $ra 		# return from main


