	.data 0x10010000
var1: 	.word 0x0083 	# var1 is a word (32 bit) with the ..
			# initial value 0x55
var2: 	.word 0x0104
var3:	.word 0x0111
var4:	.word 0x0119
first:	.byte'm'
last:	.byte'c'

	.text
	.globl main

main:	addu $16, $31, $0	# save $31 in $16
	ori $8, $0, 8
	addu $9, $0, $8
	lui $10, 4097[var1] # get the address of var1
	lui $1, 4097 [var4]	# get the address of var4
	ori $13, $1, 12 [var4]
	lui $1, 4097
	lw $12, 0($1)
	lui $1, 4097	# save var4 in register $t4
	lw $11, 12($1)
	sw $11, 0($10)
	sw $12, 0($13)
	lui $1, 4097 [var2]
	ori $10, $1, 4 [var2]
	lui $1, 4097[var3]
ori $13, $1, 8 [var3] 
lui $1, 4097
lw $12, 4($1)
lui $1, 4097
lw $11, 8($1)
sw $11, 0($10)
sw $12, 0($13)
# restore now the return address in $ra and return from main
	addu $31, $0, $16	# return address back in $31
	 jr $31 		# return from main


