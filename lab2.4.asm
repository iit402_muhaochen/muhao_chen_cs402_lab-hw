	.data 0x10010000
	var1: .word 0x0033	# var1 is a word (32 bit), ssn(33)1005
	var2: .word 0x0010	# var2 is a word (32 bit) ,ssn33(10)05
	
	ext1: .extern ext1 4		
	ext2: .extern ext2 4		
	
	.text
	.globl main
	
main: 
	lw $t0,var1	# load the value of var1 to register t0
	lw $t1,var2	# load the value of var2 to register t1	
	sw $t0,ext1	# store the value of var1 to ext1
	sw $t1,ext2	# store the value of var2 to xt2	
	li $v0,10	
	syscall		# exit