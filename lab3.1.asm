	.data
		var1: .word 3 #“3”40105
		var2: .word 4 #3“4”0105
		var3: .word -2020
	.text

	.globl main
	main:

	lw $t0, var1($0)    # $t0 = var1
	lw $t1, var2($0)    # $t1 = var2
	bne $t0, $t1, else  # if (var1 != var2), go to "else" instruction

	lw $t2, var3($0)
	sw $t2, var1($0)
	sw $t2, var2($0)
	beq $0, $0, final   # go to "final" instruction

else:
	move $t2, $t0       # swap the value of $t0 and $t1
	move $t0, $t1       
	move $t1, $t2
	sw $t0, var1($0)    # save new values to the address
	sw $t1, var2($0)

final:
	syscall
	jr $ra
