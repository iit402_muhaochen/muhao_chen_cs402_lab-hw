.data
my_array: .space 40          #initial array(space 40)
initial_value: .word 3

.text
.globl main
main:#set the address and initial value
	lw $t1, initial_value($0) # $t1(j) = initial_value
	la $t0, my_array          # $t0 =  start address
	addi $a1, $t0, 40         # $a1= end address 
loop:
	ble $a1, $t0 exit         # for (i=0; i<10, i++), exit loop 
	sw $t1, 0($t0)            # my_array[i] = j
	addi $t1, $t1, 1          # j++
	addi $t0, $t0, 4          
	j loop
exit:
	jr $ra

