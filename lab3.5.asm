.data
	msg1: .asciiz "I'm far away"
	msg2: .asciiz "I'm nearby"
	far: .word 1

.text
.globl main
main:
	li $v0, 5             # read_int
	syscall
	move $t0, $v0         # $t0 = $v0(read to tmp_re)
	li $v0, 5            # read_int
	syscall
	move $t1, $v0    #  $t1 =$v0(read to tmp_re)
	bne $t0, $t1, near #if ($t0 !=  $t1) PCnear
	li $v0, 4             
	la $a0, msg1 #print"I'm far away"
	syscall
	b far             
near:
	li $v0, 4    
	la $a0, msg2 #print"I'm nearby"
	syscall
	jr $ra

